﻿using UnityEngine;
using System.Collections;

public class LaserBeam : MonoBehaviour
{

	public int damage {get { return damage_; } private set { damage_ = value; } }

	[SerializeField]
	private string sfxName = "laser_01";

	[SerializeField]
	private float ySpeed = 20f;

	[SerializeField]
	private int damage_ = 1;

	private float yMin;
	private float yMax;

	void Start()
	{
		initMovement();
		playCreationEffects();
	}

	public void hit()
	{
		// TODO play hit effects here
		Destroy(gameObject);
	}

	private void initMovement()
	{
		Rigidbody2D rgBody = gameObject.GetComponent<Rigidbody2D>();
		if ( rgBody == null )
		{
			Debug.LogError( "Can't find a RigidBody2D on LaserBeam. LaserBeam will now be destroyed." );
			Destroy(gameObject);
			return;
		}

		rgBody.velocity = new Vector2(0, ySpeed);
	}

	private void playCreationEffects()
	{
		if (sfxName == "")
		{
			Debug.LogError("LaserBeam " + gameObject.name + "does not have a name for sfx");
			return;
		}

		SoundFXPlayer.instance.play2DClipOnce(sfxName);
	}


}