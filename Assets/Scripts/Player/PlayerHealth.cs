﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
	public int health { get {return health_;} private set { health_ = value; } }

	[SerializeField]
	private int health_ = 10;
	[SerializeField]
	private bool invinsible = false;

	private bool isDead;


	void Start()
	{
		healthSafetyCheck();
	}


	public void takeDamage(int dmg)
	{
		if (invinsible)
		{
			return;
		}
		health_ -= dmg;
		if ( health_ <= 0 )
		{
			if ( !isDead )
			{
				killPlayer();
			}
		}
	}

	private void healthSafetyCheck()
	{
		if (health_ <= 0)
		{
			Debug.Log("No health on player at spawn, deactivating player");
			gameObject.SetActive(false);
		}
		else
		{
			isDead = false;
		}
	}

	private void killPlayer()
	{
		// TODO 
		isDead = true;
		gameObject.SetActive(false);
		Debug.Log("Kill the player!");
	}


	void OnTriggerEnter2D(Collider2D coll)
	{
		// Laser hits:
		if (coll.tag == "Laser")
		{
			LaserBeam laser = coll.GetComponent<LaserBeam>();
			if ( laser == null )
			{
				Debug.LogError("An object has a laser tag but doesn't have a 'LaserBeam' component!");
			}
			else
			{
				takeDamage(laser.damage);
				laser.hit();
			}
		}
	}



}