﻿using UnityEngine;
using System.Collections;

public class PlayerFiring : MonoBehaviour
{

	[SerializeField]
	private LaserBeam normalLaser;
	[SerializeField]
	private LaserBeam specialLaser;
	[SerializeField]
	private float normalFiringRate = 0.2f;
	[SerializeField]
	private float specialFiringRate = 1.0f;
	[SerializeField]
	private int specialLaserCount = 0;
	[SerializeField]
	private bool infiniteAmmo = false;

	private bool isNormalLaserValid;
	private bool isSpecialLaserValid;


	void Start()
	{
		checkLaserValidity();
	}

	void Update()
	{
		processFiringInput();
	}

	private void processFiringInput()
	{
		// time == 0.0001f to avoid issues
		if ( Input.GetButtonDown("Fire1") )
		{
			InvokeRepeating("fireNormalLaser", 0.0001f, normalFiringRate);
		}
		if ( Input.GetButtonUp("Fire1") )
		{
			CancelInvoke("fireNormalLaser");
		}

		if ( Input.GetButtonDown("Fire2") )
		{
			InvokeRepeating("fireSpecialLaser", 0.0001f, specialFiringRate);
		}
		if ( Input.GetButtonUp("Fire2") )
		{
			CancelInvoke("fireSpecialLaser");
		}
	}

	private void fireNormalLaser()
	{
		if (isNormalLaserValid)
		{
			Vector3 startPos = transform.position + new Vector3(0f, 1f, 0f);
			LaserBeam nLaser = (LaserBeam)Instantiate( normalLaser, startPos, transform.rotation );
		}
	}

	private void fireSpecialLaser()
	{
		if (isSpecialLaserValid)
		{
			if (specialLaserCount > 0 || infiniteAmmo)
			{
				Vector3 startPos = transform.position + new Vector3(0f, 1f, 0f);
				LaserBeam sLaser = (LaserBeam)Instantiate( specialLaser, startPos, transform.rotation );
				specialLaserCount--;
			}
			else
			{
				SoundFXPlayer.instance.play2DClipOnce("no_ammo_01");
			}
		}
	}


	private void checkLaserValidity()
	{
		if ( normalLaser == null )
		{
			Debug.LogError("PlayerFiring doesn't have a normalLaser defined.");
			isNormalLaserValid = false;
		}
		else
		{
			isNormalLaserValid = true;
		}

		if ( specialLaser == null )
		{
			Debug.LogError("PlayerFiring doesn't have a specialLaser defined.");
			isSpecialLaserValid = false;
		}
		else if ( specialLaserCount < 0 )
		{
			Debug.LogError("specialLaserCount < 0 in PlayerFiring: it's not valid.");
			isSpecialLaserValid = false;
		}
		else
		{
			isSpecialLaserValid = true;
		}
	}

}
