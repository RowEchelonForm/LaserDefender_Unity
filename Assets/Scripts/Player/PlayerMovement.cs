﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : Movement
{

	override protected void move()
	{
		float xPos = Input.GetAxis("Horizontal") * xSpeed * Time.deltaTime + transform.position.x;
		float yPos = Input.GetAxis("Vertical") * ySpeed * Time.deltaTime + transform.position.y;
		clampPosToView(ref xPos, ref yPos);

		Vector3 position = new Vector3(xPos, yPos, transform.position.z);
		transform.position = position;
	}

}
