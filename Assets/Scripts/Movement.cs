﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{

	[SerializeField]
	protected float xSpeed = 10f;
	[SerializeField]
	protected float ySpeed = 10f;

	// Movement bounds
	protected float xMin;
	protected float xMax;
	protected float yMin;
	protected float yMax;

	// Use this for initialization
	virtual protected void Start()
	{
		setMovementBounds();
	}
	
	// Update is called once per frame
	virtual protected void Update()
	{
		move();
	}


	virtual protected void move()
	{
	}


	virtual protected void reverseXDirection()
	{
		xSpeed *= (-1);
	}

	virtual protected void reverseYDirection()
	{
		ySpeed *= (-1);
	}

	virtual protected void clampPosToView(ref float xPos, ref float yPos)
	{
		xPos = Mathf.Clamp(xPos, xMin, xMax);
		yPos = Mathf.Clamp(yPos, yMin, yMax);
	}

	virtual protected void setMovementBounds()
	{
		if ( Camera.main == null )
		{
			Debug.LogError("No MainCamera found. Please tag the main camera in the scene as 'MainCamera'.");
			return;
		}

		Sprite sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
		if ( sprite == null )
		{
			Debug.LogError( "Can't find a sprite renderer on the moving object: " + gameObject.ToString() );
			return;
		}

		float distanceToCamera = transform.position.z - Camera.main.transform.position.z;
		Vector3 bottomLeftPoint = Camera.main.ViewportToWorldPoint( new Vector3(0, 0, distanceToCamera) );
		Vector3 topRightPoint = Camera.main.ViewportToWorldPoint( new Vector3(1, 1, distanceToCamera) );

		Vector3 spriteMinBounds = Vector3.Scale( sprite.bounds.min, gameObject.transform.localScale );
		Vector3 spriteMaxBounds = Vector3.Scale( sprite.bounds.max, gameObject.transform.localScale );

		xMin = bottomLeftPoint.x - spriteMinBounds.x;
		xMax = topRightPoint.x - spriteMaxBounds.x;
		yMin = bottomLeftPoint.y - spriteMinBounds.y;
		yMax = topRightPoint.y - spriteMaxBounds.y;
	}
}
