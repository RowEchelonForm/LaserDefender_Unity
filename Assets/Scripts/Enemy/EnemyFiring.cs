﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFiring : MonoBehaviour
{

	[SerializeField]
	private LaserBeam laser;

	[SerializeField]
	private float fireTimer = 3f;
	[SerializeField]
	private float fireTimerRandomness = 0f;
	[SerializeField]
	private float minDelayPercentageAtStart = 10; // how big the delay percentage until enemy starts firing has to be at least
	[SerializeField]
	private float maxDelayPercentageAtStart = 100; // how big the delay percentage until enemy starts firing can to be at max
	
	private float curFireTimer = 0f;
	private float formationFireTimerDiv = 1f;

	private bool isLaserValid;
	

	void Start()
	{
		checkLaserValidity();
		checkTimerValidity();
		setFireTimerRandomness();
		setTimerAtStart();
	}

	void Update()
	{
		countFiring();
	}

	// Formation can speed up the firing percentage (via EnemyFormationObeyer)
	public void speedUpFiringByPercentage(float speedUpPercentage)
	{
		if (speedUpPercentage <= -100f)
		{
			Debug.LogError("EnemyFiring speedUpPercentage must be > -100 %");
		}
		formationFireTimerDiv = 1 + speedUpPercentage/100;
	}


	private void countFiring()
	{
		curFireTimer -= Time.deltaTime;
		if (curFireTimer <= 0)
		{
			fireLaser();
			setFireTimerRandomness();
			curFireTimer = curFireTimer/formationFireTimerDiv;
		}
	}

	private void fireLaser()
	{
		if (isLaserValid)
		{
			LaserBeam eLaser = (LaserBeam)Instantiate( laser, transform.position, Quaternion.identity );
		}
	}

	// Sets curFireTimer to be between fireTimer - fireTimerRandomness and fireTimer + fireTimerRandomness
	private void setFireTimerRandomness()
	{
		curFireTimer = Random.Range(fireTimer - fireTimerRandomness, fireTimer + fireTimerRandomness);
	}

	private void setTimerAtStart()
	{
		curFireTimer = Random.Range( curFireTimer*minDelayPercentageAtStart/100/formationFireTimerDiv,
									 curFireTimer*maxDelayPercentageAtStart/100/formationFireTimerDiv );
	}

	private void checkLaserValidity()
	{
		if ( laser == null )
		{
			Debug.LogError("EnemyFiring doesn't have a laser defined.");
			isLaserValid = false;
		}
		else
		{
			isLaserValid = true;
		}
	}

	private void checkTimerValidity()
	{
		if (fireTimer == 0)
		{
			fireTimer = 0.01f;
			Debug.LogError("fireTimer has to be > 0. Setting fireTimer to 0.01");
		}
		else if (fireTimer < 0)
		{
			fireTimer = fireTimer*(-1);
			Debug.LogWarning("fireTimer has to be > 0. Setting fireTimer to " + fireTimer);
		}
		if (fireTimerRandomness < 0)
		{
			fireTimerRandomness = fireTimerRandomness*(-1);
			Debug.LogWarning("fireTimerRandomness has to be >= 0. Setting fireTimerRandomness to " + fireTimerRandomness);
		}
		if (fireTimer - fireTimerRandomness <= 0)
		{
			fireTimerRandomness = 0f;
			Debug.LogError("fireTimer - fireTimerRandomness has to be > 0. Setting fireTimerRandomness to 0");
		}
		if (minDelayPercentageAtStart < 0)
		{
			minDelayPercentageAtStart = minDelayPercentageAtStart*(-1);
			Debug.LogWarning("minDelayPercentageAtStart has to be >= 0. Setting minDelayPercentageAtStart to " + minDelayPercentageAtStart);
		}
		if (maxDelayPercentageAtStart < 0)
		{
			maxDelayPercentageAtStart = maxDelayPercentageAtStart*(-1);
			Debug.LogWarning("maxDelayPercentageAtStart has to be >= 0. Setting maxDelayPercentageAtStart to " + maxDelayPercentageAtStart);
		}
		if (maxDelayPercentageAtStart < minDelayPercentageAtStart)
		{
			maxDelayPercentageAtStart = minDelayPercentageAtStart;
			Debug.LogWarning("maxDelayPercentageAtStart has to be >= minDelayPercentageAtStart. Setting maxDelayPercentageAtStart to " + maxDelayPercentageAtStart);
		}
	}

}
