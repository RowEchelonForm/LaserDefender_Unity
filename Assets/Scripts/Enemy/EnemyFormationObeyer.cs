﻿using UnityEngine;
using System.Collections;

public class EnemyFormationObeyer : MonoBehaviour {

	public EnemyFormationManager formationManager {get; private set;}

	public bool isAllowedToMove {get; private set;}
	public bool hasFormation {get; private set;}

	private IEnumerator enemyForceMoveCoroutine;

	private EnemyFormationSpawner spawner;
	private FormationFiring formationFiring;
	private EnemyFiring enemyFiring;
	private EnemyMovement enemyMovement;
	private Animator animator;
	SpriteRenderer thruster;

	// Use this for initialization
	void Start ()
	{
		setFormationComponents();
		setEnemyFiringComponent();
		setEnemyMovementComponent();
		setAnimatorComponent();
		getThrusterSprite();

		disableAnimatorAfterArrival();
	}

	public void moveEnemyToParentInTime(float timeToMove, FormationChanger formationChanger)
	{
		if (enemyForceMoveCoroutine != null)
		{
			StopCoroutine(enemyForceMoveCoroutine);
		}
		enemyForceMoveCoroutine = moveEnemyToParentPosition(timeToMove, formationChanger);
		StartCoroutine( enemyForceMoveCoroutine );
	}

	public void setFormationComponents()
	{
		formationManager = gameObject.GetComponentInParent<EnemyFormationManager>();
		if (formationManager == null)
		{
			hasFormation = false;
			isAllowedToMove = true;
			Debug.Log("Enemy has no formation");
		}
		else
		{
			hasFormation = true;
			isAllowedToMove = false;

			formationFiring = gameObject.GetComponentInParent<FormationFiring>();
			if (formationFiring == null)
			{
				Debug.LogError("Enemy has no FormationFiring but is in a formation!");
			}
			updateEnemyFiringRate();
		}

	}

	public void setFreeFromFormation()
	{
		disableAnimatorInTime(0f);
		formationManager = null;
		hasFormation = false;
		isAllowedToMove = true;
		transform.parent = null;
		if (enemyForceMoveCoroutine != null)
		{
			StopCoroutine(enemyForceMoveCoroutine);
		}
		enemyMovement.setPanicMovement();
		updateEnemyFiringRate();
	}

	// Can only be done once
	public void initEnemySpawner(EnemyFormationSpawner enemySpawner)
	{
		if (spawner != null)
		{
			Debug.LogError("Can't init spawner in EnemtFormationObeyer: Enemy already has a spawner set.");
			return;
		}
		spawner = enemySpawner;
		if (spawner == null)
		{
			Debug.LogWarning("Enemy does not have a valid spawner set!");
			return;
		}
		spawner.enemyWasSpawned( gameObject.GetInstanceID() );
	}



	private void setEnemyMovementComponent()
	{
		enemyMovement = gameObject.GetComponent<EnemyMovement>();
		if (enemyMovement == null)
		{
			Debug.LogError("Error in EnemyFormationObeyer: Enemy has no EnemyMovement component!");
		}
	}

	private void setAnimatorComponent()
	{
		animator = gameObject.GetComponent<Animator>();
		if (animator == null)
		{
			Debug.LogError("Error in EnemyFormationObeyer: Enemy has no Animator component!");
		}
	}

	private void setEnemyFiringComponent()
	{
		enemyFiring = gameObject.GetComponent<EnemyFiring>();
		if (enemyFiring == null)
		{
			Debug.LogError("Error in EnemyFormationObeyer: Enemy has no EnemyFiring component!");
		}
		updateEnemyFiringRate();
	}

	private void getThrusterSprite()
	{
		int childCount = transform.childCount;
		for (int i = 0; i < childCount; ++i)
		{
			Transform child = transform.GetChild(i);
			if (child.tag == "Thruster")
			{
				thruster = child.GetComponent<SpriteRenderer>();
				break;
			}
		}

		if (thruster == null)
		{
			Debug.LogError("Error in EnemyFormationObeyer: Enemy has no thruster sprite as a child! Remember to tag the thruster as 'Thruster'.");
		}
	}

	private void updateEnemyFiringRate()
	{
		if (enemyFiring != null)
		{
			if (hasFormation && formationFiring != null)
			{
				enemyFiring.speedUpFiringByPercentage( formationFiring.getFiringSpeedUp() );
			}
			else // no formation
			{
				enemyFiring.speedUpFiringByPercentage( 0f );
			}
		}
	}

	private void disableAnimatorAfterArrival()
	{
		AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
		float animLength = clipInfo[0].clip.length;
		StartCoroutine( disableAnimatorInTime( animLength/animator.speed ) );
	}

	private IEnumerator disableAnimatorInTime(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		yield return null; // one extra frame
		disableAnimator();
	}

	private void disableAnimator() // resets rotation and thruster to default values as well
	{
		animator.enabled = false;
		transform.rotation = Quaternion.identity;
		if (thruster != null)
		{
			thruster.color = new Color(thruster.color.r, thruster.color.g, thruster.color.b, 0.2f); // 20%
		}
	}

	private IEnumerator moveEnemyToParentPosition(float timeToMove, FormationChanger formationChanger)
	{
		disableAnimator();
		float timeUsed = 0f;
		float distanceToParent = Mathf.Sqrt( Mathf.Pow(transform.localPosition.x, 2) + Mathf.Pow(transform.localPosition.y, 2) );
		float speed = distanceToParent / timeToMove;
		float distanceToTravelOnFrame = 0f;
		Vector3 parentPos = new Vector3(0, 0, 0);

		while (timeUsed < timeToMove)
		{
			timeUsed += Time.deltaTime;
			distanceToTravelOnFrame = Time.deltaTime*speed;
			transform.localPosition = Vector3.MoveTowards(transform.localPosition, parentPos, distanceToTravelOnFrame);
			yield return null;
		}
		if ( transform.localPosition != parentPos )
		{
			Debug.Log("Enemy didn't reach it's target in time, teleporting to it");
			transform.localPosition = parentPos;
		}
		formationChanger.enemyReachedGoalPosition();
	}



	void OnDestroy()
	{
		if (hasFormation)
		{
			formationManager.enemyDestroyed(gameObject);
		}

		if (spawner == null)
		{
			Debug.LogWarning( "An enemy that's being destroyed does not have a spawner defined! " +
							  "A spawner is not informed of the enemy being destroyed" );
			return;
		}
		spawner.enemyWasDestroyed( gameObject.GetInstanceID() );
	}

}
