﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyFormationMovement : MonoBehaviour
{

	[SerializeField]
	private float xFormationSpeed = 5f;
	[SerializeField]
	private float yFormationSpeed = 0f;
	[SerializeField]
	private bool randomStartingDirection = true;

	private bool isMovingRight;
	private bool isMovingUp;
	private bool isQuitting = false;
	private bool isInitialized = false;

	// Bounds for the formation:
	private float xMin = 0;
	private float xMax = 0;
	private float yMin = 0;
	private float yMax = 0;

	// Bounds for where the formation (the pivot point of formation) is allowed to move:
	private float levelMinX;
	private float levelMaxX;
	private float levelMinY;
	private float levelMaxY;

	
	// Update is called once per frame
	void Update()
	{
		if (isInitialized)
		{
			moveFormation();
		}
	}

	public void formationWasFullyInitialized()
	{
		if (isInitialized)
		{
			Debug.LogError("Can't initialize EnemyFormationMovement more than once");
			return;
		}
		setMovementLimits();
		initMovementDirection();
		isInitialized = true;
	}

	public void enemyWasAddedToFormation(GameObject enemy, ref bool isFirst)
	{
		updateFormationBounds(enemy, ref isFirst);
	}

	public void enemyWasRemovedFromFormation(List<GameObject> allEnemies)
	{
		resizeFormation(allEnemies);
	}

	// Works only before initialization
	public void setSpeeds(float xSpeed, float ySpeed)
	{
		if ( isInitialized )
		{
			Debug.LogError("Can't set formation speeds after initializing the formation");
			return;
		}
		xFormationSpeed = xSpeed;
		yFormationSpeed = ySpeed;
		randomStartingDirection = false;
	}

	public float getXSpeed()
	{
		return xFormationSpeed;
	}

	public float getYSpeed()
	{
		return yFormationSpeed;
	}



	private void moveFormation()
	{
		float xPos = xFormationSpeed * Time.deltaTime + transform.position.x;
		float yPos = yFormationSpeed * Time.deltaTime + transform.position.y;
		if ( (xPos >= levelMaxX && isMovingRight) || (xPos <= levelMinX && !isMovingRight) )
		{
			reverseMovementDirectionX(ref xPos);
		}
		if ( (yPos >= levelMaxY && isMovingUp) || (yPos <= levelMinY && !isMovingUp) )
		{
			reverseMovementDirectionY(ref yPos);
		}
		Vector3 position = new Vector3(xPos, yPos, transform.position.z);
		transform.position = position;
	}

	private void reverseMovementDirectionX(ref float xPos)
	{
		xFormationSpeed *= (-1); // reverse
		isMovingRight = !isMovingRight;
		xPos = xFormationSpeed * Time.deltaTime + transform.position.x; // re-calculate
	}

	private void reverseMovementDirectionY(ref float yPos)
	{
		yFormationSpeed *= (-1); // reverse
		isMovingUp = !isMovingUp;
		yPos = yFormationSpeed * Time.deltaTime + transform.position.y; // re-calculate
	}

	private void resizeFormation(List<GameObject> allEnemies)
	{
		if (!isQuitting)
		{
			bool isFirst = true;
			for (int i = 0; i < allEnemies.Count; ++i)
			{
				updateFormationBounds(allEnemies[i], ref isFirst);
			}
			setMovementLimits();
		}
	}

	// Updates the bounds for the enemies in the formation.
	// Should be called after a new enemy has been spawned in the formation.
	private void updateFormationBounds(GameObject enemy, ref bool isFirst)
	{
		if ( !enemy.GetComponent<SpriteRenderer>() )
		{
			Debug.LogError("No SpriteRenderer on enemy!");
			return;
		}
		Bounds bounds = enemy.GetComponent<SpriteRenderer>().sprite.bounds;
		bounds.min = Vector3.Scale( bounds.min, enemy.transform.localScale );
		bounds.max = Vector3.Scale( bounds.max, enemy.transform.localScale );

		if (isFirst)
		{
			xMin = enemy.transform.parent.position.x + bounds.min.x;
			xMax = enemy.transform.parent.position.x + bounds.max.x;
			yMin = enemy.transform.parent.position.y + bounds.min.y;
			yMax = enemy.transform.parent.position.y + bounds.max.y;
			isFirst = false;
			return;
		}
		if ( enemy.transform.parent.position.x + bounds.min.x < xMin )	{ xMin = enemy.transform.parent.position.x + bounds.min.x; }
		if ( enemy.transform.parent.position.x + bounds.max.x > xMax )	{ xMax = enemy.transform.parent.position.x + bounds.max.x; }
		if ( enemy.transform.parent.position.y + bounds.min.y < yMin )	{ yMin = enemy.transform.parent.position.y + bounds.min.y; }
		if ( enemy.transform.parent.position.y + bounds.max.y > yMax )	{ yMax = enemy.transform.parent.position.y + bounds.max.y; }
	}

	// Sets movement limits for the whole formation.
	// The size/bounds of the formation must be set before.
	// Call when formation bounds have beed updated.
	private void setMovementLimits()
	{
		if ( Camera.main == null )
		{
			Debug.LogError("No MainCamera found. Please tag the main camera in the scene as 'MainCamera'.");
			return;
		}

		float distanceToCamera = transform.position.z - Camera.main.transform.position.z;
		Vector3 bottomLeftPoint = Camera.main.ViewportToWorldPoint( new Vector3(0, 0, distanceToCamera) );
		Vector3 topRightPoint = Camera.main.ViewportToWorldPoint( new Vector3(1, 1, distanceToCamera) );

		levelMinX = bottomLeftPoint.x - (xMin - transform.position.x);
		levelMaxX = topRightPoint.x - (xMax - transform.position.x);
		levelMinY = bottomLeftPoint.y - (yMin - transform.position.y);
		levelMaxY = topRightPoint.y - (yMax - transform.position.y);
	}

	// Based on xFormationSpeed and yFormationSpeed
	private void initMovementDirection()
	{
		if (randomStartingDirection)
		{
			randomizeMovementDirection();
		}

		if (xFormationSpeed > 0)
		{
			isMovingRight = true;
		}
		else
		{
			isMovingRight = false;
		}
		if (yFormationSpeed > 0)
		{
			isMovingUp = true;
		}
		else
		{
			isMovingUp = false;
		}
	}

	private void randomizeMovementDirection()
	{
		int up = Random.Range(0, 2);
		int right = Random.Range(0, 2);
		if ( up == 0 )
		{
			yFormationSpeed *= (-1);
		}
		if ( right == 0 )
		{
			xFormationSpeed *= (-1);
		}
	}

	void OnApplicationQuit()
	{
		isQuitting = true;
	}




	/*// Calculates what the formation position will be in 'time' seconds.
	public Vector3 calculateFuturePosition(float time)
	{
		if (!isInitialized)
		{
			Debug.LogError( "Can't calculate future position for enemy formation because " +
						    "'EnemyFormationMovement' class instance isn't initialized correctly" );
			return transform.position;
		}
		float xPos = xFormationSpeed * time + transform.position.x;
		float yPos = yFormationSpeed * time + transform.position.y;

		float tempDiff = 0f;
		bool isPosOk = false;

		while ( !isPosOk )
		{
			isPosOk = true;
			if (xPos < levelMinX)
			{
				tempDiff = levelMinX - xPos;
				xPos = levelMinX + tempDiff;
				isPosOk = false;
			}
			else if (xPos > levelMaxX)
			{
				tempDiff = xPos - levelMaxX;
				xPos = levelMaxX - tempDiff;
				isPosOk = false;
			}
			if (yPos < levelMinY)
			{
				tempDiff = levelMinY - yPos;
				yPos = levelMinY + tempDiff;
				isPosOk = false;
			}
			else if (yPos > levelMaxY)
			{
				tempDiff = yPos - levelMaxY;
				yPos = levelMaxY - tempDiff;
				isPosOk = false;
			}
		}
		return new Vector3(xPos, yPos, transform.position.z);
	}*/
}
