﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : Movement
{
	[SerializeField]
	protected bool randomStartingDirection = true;
	[SerializeField]
	protected float randomSpeedMaxPercent = 10f; // how much speed can differ at max (+-)

	protected bool isMovingRight;
	protected bool isMovingUp;

	private EnemyFormationObeyer formationObeyer;


	// Use this for initialization
	override protected void Start()
	{
		base.Start();
		setFormationObeyer();
		initMovementDirection();
		randomizeSpeed();
	}

	public void setPanicMovement()
	{
		randomStartingDirection = true;
		initMovementDirection();
	}

	protected override void move()
	{
		if (formationObeyer.isAllowedToMove)
		{
			float xPos = xSpeed * Time.deltaTime + transform.position.x;
			float yPos = ySpeed * Time.deltaTime + transform.position.y;
			if ( (xPos >= xMax && isMovingRight) || (xPos <= xMin && !isMovingRight) )
			{
				reverseXDirection();
				xPos = xSpeed * Time.deltaTime + transform.position.x; // recalc
			}
			if ( (yPos >= yMax && isMovingUp) || (yPos <= yMin && !isMovingUp) )
			{
				reverseYDirection();
				yPos = ySpeed * Time.deltaTime + transform.position.y; // recalc
			}
			Vector3 position = new Vector3(xPos, yPos, transform.position.z);
			transform.position = position;
		}
	}

	protected override void reverseXDirection()
	{
		base.reverseXDirection();
		isMovingRight = !isMovingRight;
	}

	protected override void reverseYDirection()
	{
		base.reverseYDirection();
		isMovingUp = !isMovingUp;
	}


	// Initializes movement directions (randomizes if needed)
	protected void initMovementDirection()
	{
		if (randomStartingDirection)
		{
			randomizeMovementDirection();
		}

		if (xSpeed > 0)
		{
			isMovingRight = true;
		}
		else
		{
			isMovingRight = false;
		}

		if (ySpeed > 0)
		{
			isMovingUp = true;
		}
		else
		{
			isMovingUp = false;
		}
	}

	protected void randomizeMovementDirection()
	{
		int up = Random.Range(0, 2);
		int right = Random.Range(0, 2);
		if ( up == 0 )
		{
			ySpeed *= (-1);
		}
		if ( right == 0 )
		{
			xSpeed *= (-1);
		}
	}

	protected void randomizeSpeed()
	{
		float randomAmount = Random.Range(-randomSpeedMaxPercent, randomSpeedMaxPercent)/100;
		xSpeed += xSpeed * randomAmount;
		ySpeed += ySpeed * randomAmount;
	}

	private void setFormationObeyer()
	{
		formationObeyer = gameObject.GetComponentInParent<EnemyFormationObeyer>();
		if (formationObeyer == null)
		{
			Debug.LogError("Error: Enemy has no formation obeyercomponent");
		}
	}

}
