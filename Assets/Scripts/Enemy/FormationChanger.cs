﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FormationChanger : MonoBehaviour
{
	
	private EnemyFormationManager nextFormation;

	private List<EnemyFormationObeyer> transferedEnemies;
	private int numOfEnemiesMovingToFormation;

	// An enemy calls this when it reaches its goal position in 'nextFormation' or when it no longer needs to
	// move to the goal position set by this class instance.
	public void enemyReachedGoalPosition()
	{
		if (numOfEnemiesMovingToFormation < 0)
		{
			Debug.LogError("Error in FormationChanger: More than the number of transfered enemies have signaled " +
						   "that they moved to their positions in next formation");
			return;
		}
		else
		{
			--numOfEnemiesMovingToFormation;
		}

		if (numOfEnemiesMovingToFormation == 0)
		{
			Debug.Log("All enemies have reached their goal postion, deleting FormationChanger");
			Destroy(gameObject);
		}
	}

	// Moves enemies from 'currentFormation' to next formation. Returns true if success.
	// Both formations must be initialized and active/enabled full formation gameobjects (have the other necessary components).
	// Destroys itself after a while if success. If fails, needs to be destroyed from outside.
	public bool changeFormation( EnemyFormationManager currentFormationManager, EnemyFormationManager nextFormationManager, float formationChangeTime)
	{
		initVariables();
		nextFormation = nextFormationManager;
		List<Position> nextPositions = new List<Position>();
		if ( !fillFormationPositionList(ref nextPositions, nextFormation) )
		{
			return false;
		}

		List<GameObject> normalEnemies = currentFormationManager.normalEnemiesToList();
		List<GameObject> bossEnemies = currentFormationManager.bossEnemiesToList();
		if ( normalEnemies.Count + bossEnemies.Count > nextPositions.Count )
		{
			Debug.LogError("Error: next formation doesn't have the required number of enemy positions. Needed positions: "
						   + (bossEnemies.Count+normalEnemies.Count) + ", actual positions: " + nextPositions.Count);
			return false;
		}
		else
		{
			bool wasSuccess = transferEnemies( nextFormation, nextPositions, normalEnemies, bossEnemies );
			if ( wasSuccess )
			{
				setNextFormationSpeed(currentFormationManager, nextFormation);
				nextFormation.setAsInitialized();
				moveTransferedEnemiesToNewPositions(formationChangeTime);
				StartCoroutine( destroyFormationChanger(formationChangeTime) );
			}
			return wasSuccess;
		}

	}

	// Fills the list 'nextPositions' with valid Positions that are children of the transform of 'nextFormation'.
	// 'nextPositions' and 'nextFormation' must exist AND be initialized.
	private bool fillFormationPositionList( ref List<Position> nextPositions, EnemyFormationManager nextFormation )
	{
		foreach ( Transform child in nextFormation.gameObject.transform )
		{
			if ( child.tag != "Position" )
			{
				continue;
			}
			Position nextSpawnPos = child.gameObject.GetComponent<Position>();
			if ( !spawnPositionValidityCheck(nextSpawnPos) )
			{
				Debug.LogError("Error: can't transfer enemies to next formation: some positions in next formation are invalid");
				return false;
			}
			else
			{
				nextPositions.Add(nextSpawnPos);
			}
		}
		return true;
	}

	// Transfers the enemies to the 'nextPositions' under 'nextFormation'.
	// Does NOT move the enemies' transforms physically to the same transform as their parent Position
	private bool transferEnemies( EnemyFormationManager nextFormation, List<Position> nextPositions, List<GameObject> normalEnemies,
								  List<GameObject> bossEnemies )
	{
		bool isFirst = true;
		bool isEnemyBoss;
		for (int i = 0; i < nextPositions.Count; ++i)
		{
			GameObject enemyToTransfer;
			if ( nextPositions[i].isBossPos && bossEnemies.Count > 0 )
			{
				enemyToTransfer = bossEnemies[0];
				isEnemyBoss = true;
			}
			else if ( normalEnemies.Count > 0 )
			{
				enemyToTransfer = normalEnemies[0];
				isEnemyBoss = false;
			}
			else if ( normalEnemies.Count <= 0 && bossEnemies.Count > 0 )
			{
				enemyToTransfer = bossEnemies[0];
				isEnemyBoss = true;
			}
			else if ( normalEnemies.Count + bossEnemies.Count <= 0 ) // more positions than enemies => end transfer
			{
				return true;
			}
			else
			{
				Debug.LogError("Unknown error in formation changer");
				return false;
			}

			nextFormation.addEnemyInFormationPosition(enemyToTransfer, nextPositions[i], isEnemyBoss, ref isFirst);
			EnemyFormationObeyer enemyObeyer = enemyToTransfer.GetComponent<EnemyFormationObeyer>();

			if (isEnemyBoss)
			{
				bossEnemies.RemoveAt(0);
			}
			else
			{
				normalEnemies.RemoveAt(0);
			}
			transferedEnemies.Add( enemyObeyer );

		}
		return true;
	}

	private void setNextFormationSpeed(EnemyFormationManager currentFormationManager, EnemyFormationManager nextFormation)
	{
		if ( !currentFormationManager.doesCarrySpeedToNextFormation() )
		{
			return;
		}

		EnemyFormationMovement nextMov = nextFormation.GetComponent<EnemyFormationMovement>();
		if ( nextMov == null )
		{
			return;
		}

		float xSpeed = currentFormationManager.getXSpeed();
		float ySpeed = currentFormationManager.getYSpeed();
		nextMov.setSpeeds(xSpeed, ySpeed);
	}

	// Commands the transfered enemies to move to their new formation positions.
	private void moveTransferedEnemiesToNewPositions(float formationChangeTime)
	{
		for (int i = 0; i < transferedEnemies.Count; ++i)
		{
			transferedEnemies[i].moveEnemyToParentInTime(formationChangeTime, this);
			++numOfEnemiesMovingToFormation;
		}
	}

	// Destroys the formation changer safely after 'waitTime'.
	// waitTime is the time given to enemies to move to their goal positions.
	private IEnumerator destroyFormationChanger(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		yield return 0; // one extra frame
		if ( numOfEnemiesMovingToFormation != 0 )
		{
			checkTransferedEnemies();
		}
		if ( numOfEnemiesMovingToFormation > 0 )
		{
			Debug.LogError("Some enemies have not reached their goal positions properly in time");
		}
		Destroy(gameObject);
	}

	private void checkTransferedEnemies()
	{
		for ( int i = 0; i < transferedEnemies.Count; ++i )
		{
			if ( transferedEnemies[i] == null )
			{
				--numOfEnemiesMovingToFormation;
				Debug.Log("Enemy was destroyed before it reached its goal position in the next formation.");
			}
			else if ( !transferedEnemies[i].hasFormation )
			{
				--numOfEnemiesMovingToFormation;
				Debug.Log("Enemy formation was broken before the enemy reached its goal position in that formation.");
			}
			else if ( transferedEnemies[i].formationManager != nextFormation )
			{
				--numOfEnemiesMovingToFormation;
				Debug.Log("Enemy was moved to another formation before it reached its goal position in the previous formation.");
			}
		}
	}

	// Checks if 'spawnPos' is valid and an enemy can be spawned on it
	private bool spawnPositionValidityCheck(Position spawnPos)
	{
		if ( spawnPos == null )
		{
			Debug.LogError("Error: A postion in next enemy formation doens't have a Position component!");
			return false;
		}
		if ( spawnPos.isPositionTaken() )
		{
			Debug.Log("Spawn position already has an enemy, new enemy not spawned in next formation.");
			return false;
		}
		return true;
	}

	private void initVariables()
	{
		numOfEnemiesMovingToFormation = 0;
		transferedEnemies = new List<EnemyFormationObeyer>();
	}
}
