﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyFormationManager : MonoBehaviour
{
	
	public bool spawnEmpty = false;
	public GameObject enemyPrefab { get { return enemyPrefab_; } private set { enemyPrefab_ = value; } }
	public GameObject bossEnemyPrefab { get { return bossEnemyPrefab_; } private set { bossEnemyPrefab_ = value; } }

	[SerializeField]
	private GameObject enemyPrefab_;
	[SerializeField]
	private GameObject bossEnemyPrefab_;

	[SerializeField]
	private GameObject nextFormationPrefab; // if null, formation breaks when 'formationChangeLimit' is reached
	[SerializeField]
	public bool sameSpeedInNextFormations = true;
	[SerializeField]
	private uint formationChangeLimit = 0; // when will the formation change to 'nextFormation'
	[SerializeField]
	private float formationChangeTime = 1f;
	[SerializeField]
	private bool alwaysBreakIfBossesKilled = true; // will always break the formation if all bosses are killed
	[SerializeField]
	private EnemyFormationSpawner spawner;

	[SerializeField]
	private EnemyFormationMovement formationMovement;

	private Dictionary<int, GameObject> normalEnemiesInFormation; // key == instanceID, value == enemy GameObject
	private Dictionary<int, GameObject> bossEnemiesInFormation; // key == instanceID, value == bossEnemy GameObject
	private EnemyFormationManager nextFormation;
	private bool changeFormationPointReached;
	private bool breakFormationPointReached;


	void Start()
	{
		initFormation();
	}

	void LateUpdate()
	{
		if (breakFormationPointReached)
		{
			breakFormation();
		}
		else if (changeFormationPointReached)
		{
			changeFormation();
		}
	}

	public bool isFormationEmpty()
	{
		if ( bossEnemiesInFormation.Count == 0 && normalEnemiesInFormation.Count == 0 )
		{
			return true;
		}
		return false;
	}

	public bool doesCarrySpeedToNextFormation()
	{
		return sameSpeedInNextFormations;
	}

	public float getXSpeed()
	{
		return formationMovement.getXSpeed();
	}

	public float getYSpeed()
	{
		return formationMovement.getYSpeed();
	}

	// Adds an already spawned enemy to this formation to 'position'
	public bool addEnemyInFormationPosition(GameObject enemy, Position position, bool isBoss, ref bool isFirst)
	{
		if (position.transform.parent.GetInstanceID() != transform.GetInstanceID() )
		{
			Debug.LogError("The position that an enemy was supposed to be added to isn't a child of this formation");
			return false;
		}
		if ( position.isPositionTaken() )
		{
			Debug.LogError("The position that an enemy was supposed to be added to already has an enemy");
			return false;
		}

		if (isBoss)
		{
			bossEnemiesInFormation.Add( enemy.GetInstanceID(), enemy );
		}
		else
		{
			normalEnemiesInFormation.Add( enemy.GetInstanceID(), enemy );
		}
		enemy.transform.SetParent( position.transform, true);
		position.setPositionAsTaken(true);
		formationMovement.enemyWasAddedToFormation(enemy, ref isFirst);
		enemy.GetComponent<EnemyFormationObeyer>().setFormationComponents();
		return true;
	}

	// Formation will now think it's initialized (move around)
	public void setAsInitialized()
	{
		formationMovement.formationWasFullyInitialized();
	}

	// Call when an enemy in the formation is destroyed
	public void enemyDestroyed(GameObject enemy)
	{
		if ( enemy.tag != "Enemy" )
		{
			Debug.LogError("Enemy can't be destroyed: It's not tagged as 'Enemy'.");
			return;
		}
		removeEnemy(enemy);
	}

	// Returns a list containing all the enemies in the formation (including bosses)
	public List<GameObject> allEnemiesToList()
	{
		List<GameObject> allEnemies = new List<GameObject>( normalEnemiesInFormation.Count + bossEnemiesInFormation.Count );

		foreach ( KeyValuePair<int, GameObject> enemy in normalEnemiesInFormation )
		{
			allEnemies.Add(enemy.Value);
		}
		foreach ( KeyValuePair<int, GameObject> enemy in bossEnemiesInFormation )
		{
			allEnemies.Add(enemy.Value);
		}
		return allEnemies;
	}

	public List<GameObject> bossEnemiesToList()
	{
		List<GameObject> bossEnemies = new List<GameObject>( bossEnemiesInFormation.Count );
		foreach ( KeyValuePair<int, GameObject> enemy in bossEnemiesInFormation )
		{
			bossEnemies.Add(enemy.Value);
		}
		return bossEnemies;
	}

	public List<GameObject> normalEnemiesToList()
	{
		List<GameObject> normalEnemies = new List<GameObject>( normalEnemiesInFormation.Count );
		foreach ( KeyValuePair<int, GameObject> enemy in normalEnemiesInFormation )
		{
			normalEnemies.Add(enemy.Value);
		}
		return normalEnemies;
	}

	// Sets the spawner that activated this formation (or the previous formation). Can only be done once.
	public void initEnemySpawner(EnemyFormationSpawner enemySpawner)
	{
		if (spawner != null)
		{
			Debug.LogError("Can't init spawner in EnemyFormationManager: It already has a spawner set.");
			return;
		}
		spawner = enemySpawner;
	}


	private void initFormation()
	{
		normalEnemiesInFormation = new Dictionary<int, GameObject>();
		bossEnemiesInFormation = new Dictionary<int, GameObject>();
		connectFormationMovementComponent();
		changeFormationPointReached = false;
		breakFormationPointReached = false;
		initNextFormation();
		if (!spawnEmpty)
		{
			spawnEnemiesInFormation();
			formationMovement.formationWasFullyInitialized();
		}
	}

	// Initializes/creates the 'nextFormation' spawns empty) if 'nextFormationPrefab' isn't null
	private void initNextFormation()
	{
		if ( nextFormationPrefab != null && nextFormationPrefab.tag != "Formation" )
		{
			Debug.LogError("Error: Next formation prefab is missing 'Formation' tag. Check that the formation prefab is viable.");
			nextFormationPrefab = null;
		}
		else if ( nextFormationPrefab != null )
		{
			nextFormationPrefab.GetComponent<EnemyFormationManager>().spawnEmpty = true;
			GameObject nextFormationObject = GameObject.Instantiate( nextFormationPrefab, transform.position, transform.rotation ) as GameObject;
			nextFormation = nextFormationObject.GetComponent<EnemyFormationManager>();
			nextFormationObject.transform.SetParent(transform); // next formation will be a child of this formation
			nextFormation.sameSpeedInNextFormations = sameSpeedInNextFormations;
			nextFormation.initEnemySpawner(spawner);
		}
	}

	private void spawnEnemiesInFormation()
	{
		bool isFirst = true;
		foreach ( Transform child in transform )
		{
			if (child.tag == "Position")
			{
				Position spawnPos = child.gameObject.GetComponent<Position>();
				if ( spawnPositionValidityCheck(spawnPos) )
				{
					spawnAddEnemyInFormationPosition(ref isFirst, spawnPos);
				}
			}
		}
	}

	// Spawns an enemy at this formation position point and updates formation bounds if needed
	private GameObject spawnAddEnemyInFormationPosition(ref bool isFirst, Position spawnPos)
	{
		GameObject enemy = spawnAddEnemyWithOverride(spawnPos.enemyPrefabOverride, spawnPos.isBossPos, spawnPos);
		if (enemy == null)
		{
			return null;
		}
		enemy.transform.SetParent( spawnPos.transform, true);
		spawnPos.setPositionAsTaken(true);
		formationMovement.enemyWasAddedToFormation(enemy, ref isFirst);

		return enemy;
	}

	// Checks if 'spawnPos' is valid and an enemy can be spawned on it
	private bool spawnPositionValidityCheck(Position spawnPos)
	{
		if ( spawnPos == null )
		{
			Debug.LogError("Error: A postion in enemy formation doesn't have a Position component!");
			return false;
		}
		if ( spawnPos.tag != "Position" )
		{
			Debug.LogError("An incorrectly tagged position in enemy formation");
			return false;
		}
		if ( spawnPos.isPositionTaken() )
		{
			Debug.Log("Spawn position already has an enemy, new enemy not spawned.");
			return false;
		}
		return true;
	}

	// Spawns the enemy and adds it to the correct dictionary.
	// If 'enemyPrefabOverride' is null, spawns an enemy with the default prefab ('enemyPrefab_' or 'bossEnemyPrefab_')
	private GameObject spawnAddEnemyWithOverride( GameObject enemyPrefabOverride, bool isBoss, Position position )
	{
		GameObject enemy;
		EnemyFormationObeyer obeyer;
		if ( isBoss ) // spawn boss enemy
		{
			if (enemyPrefabOverride == null)
			{
				enemyPrefabOverride = bossEnemyPrefab_;
			}
			enemy = spawnEnemy(enemyPrefabOverride, position);
			if ( enemy == null )
			{
				Debug.LogError("Can't spawn boss enemy!");
				return null;
			}

			obeyer = enemy.GetComponent<EnemyFormationObeyer>();
			if (obeyer == null)
			{
				Debug.LogError("Error: Spawned enemy doesn't have 'EnemyFormationObeyer' component!");
				return null;
			}
			bossEnemiesInFormation.Add( enemy.GetInstanceID(), enemy );
		}
		else // spawn normal enemy
		{
			if (enemyPrefabOverride == null)
			{
				enemyPrefabOverride = enemyPrefab_;
			}
			enemy = spawnEnemy(enemyPrefabOverride, position);
			if ( enemy == null )
			{
				Debug.LogError("Can't spawn normal enemy!");
				return null;
			}

			obeyer = enemy.GetComponent<EnemyFormationObeyer>();
			if (obeyer == null)
			{
				Debug.LogError("Error: Spawned enemy doesn't have 'EnemyFormationObeyer' component!");
				return null;
			}
			normalEnemiesInFormation.Add( enemy.GetInstanceID(), enemy );
		}
		obeyer.initEnemySpawner(spawner);
		return enemy;
	}

	// Spawns the enemy as defined in enemyObjectPrefab
	private GameObject spawnEnemy(GameObject enemyObjectPrefab, Position position)
	{
		if (enemyObjectPrefab == null )
		{
			Debug.LogError("No enemy prefab given: can't spawn enemy");
			return null;
		}
		else if (enemyObjectPrefab.tag != "Enemy")
		{
			Debug.LogError("Enemy prefab does not have an 'Enemy' tag: can't spawn enemy");
			return null;
		}
		//GameObject enemy = Instantiate( enemyObjectPrefab, position, Quaternion.identity ) as GameObject;
		GameObject enemy = Instantiate( enemyObjectPrefab, position.transform ) as GameObject;
		return enemy;
	}

	private void removeEnemy(GameObject enemy)
	{
		if ( bossEnemiesInFormation.ContainsKey(enemy.GetInstanceID()) )
		{
			removeBossEnemy(enemy);
		}
		else if ( normalEnemiesInFormation.ContainsKey(enemy.GetInstanceID()) )
		{
			removeNormalEnemy(enemy);
		}
		else // can't find enemy in formation
		{
			Debug.LogError("The enemy is not part of this formation. Enemy instanceID: " + enemy.GetInstanceID() );
			return;
		}

		Position enemyPos = enemy.transform.parent.GetComponent<Position>();
		if ( enemyPos == null )
		{
			Debug.Log("Enemy in formation was not set on a formation position!");
			return;
		}
		enemyPos.setPositionAsTaken( false );

		formationMovement.enemyWasRemovedFromFormation( allEnemiesToList() );
	}

	// enemy MUST exist in 'normalEnemiesInFormation'
	private void removeNormalEnemy(GameObject enemy)
	{
		normalEnemiesInFormation.Remove( enemy.GetInstanceID() );
		if ( normalEnemiesInFormation.Count + bossEnemiesInFormation.Count <= formationChangeLimit )
		{
			Debug.Log("Formation change count achieved, formation will change");
			changeFormationPointReached = true;
		}
	}

	// enemy MUST exist in 'bossEnemiesInFormation'
	private void removeBossEnemy(GameObject enemy)
	{
		bossEnemiesInFormation.Remove( enemy.GetInstanceID() );
		Debug.Log("Boss killed");
		if ( bossEnemiesInFormation.Count == 0 && alwaysBreakIfBossesKilled )
		{
			Debug.Log("All bosses killed, formation breaks");
			breakFormationPointReached = true;
		}
		else if ( normalEnemiesInFormation.Count + bossEnemiesInFormation.Count <= formationChangeLimit )
		{
			Debug.Log("Formation change count achieved, formation changes");
			changeFormationPointReached = true;
		}
	}

	// Only called from 'LateUpdate()' !!!
	private void changeFormation()
	{
		if (!changeFormationPointReached)
		{
			return;
		}
		if ( nextFormation == null || normalEnemiesInFormation.Count + bossEnemiesInFormation.Count <= 0)
		{
			Debug.Log("No next formation or enemies in this formation => breaking this formation instead");
			changeFormationPointReached = false;
			breakFormationPointReached = true;
			breakFormation();
			return;
		}

		nextFormation.transform.SetParent(null);
		GameObject changerObject = new GameObject("Formation Changer");
		FormationChanger changer = changerObject.AddComponent<FormationChanger>();
		changeFormationPointReached = false;
		if ( !changer.changeFormation(this, nextFormation, formationChangeTime) )
		{
			Destroy(changer, formationChangeTime);
			Destroy(nextFormation.gameObject);
			Debug.LogError("Error: Formation change failed => breaking formation instead");
			breakFormationPointReached = true;
			breakFormation();
			return;
		}
		Destroy(gameObject);
	}

	// Only called from 'LateUpdate()' or 'changeFormation()' (which is only called from 'LateUpdate()') !!!
	private void breakFormation()
	{	
		if (!breakFormationPointReached)
		{
			return;
		}

		breakFormationPointReached = true;
		if ( normalEnemiesInFormation.Count == 0 && bossEnemiesInFormation.Count == 0 )
		{
			Debug.Log("All enemies in formation have been destroyed, destroying the formation.");
			Destroy(gameObject);
			return;
		}
		setEnemiesFree();
		Destroy(gameObject);
	}

	private void setEnemiesFree()
	{
		foreach ( KeyValuePair<int, GameObject> enemy in normalEnemiesInFormation )
		{
			if (enemy.Value == null)
			{
				Debug.LogError("Unknown error: Some enemies in formation are null when trying to set them free!");
			}
			enemy.Value.GetComponent<EnemyFormationObeyer>().setFreeFromFormation();
		}
		foreach ( KeyValuePair<int, GameObject> enemy in bossEnemiesInFormation )
		{
			if (enemy.Value == null)
			{
				Debug.LogError("Unknown error: Some enemies in formation are null when trying to set them free!");
			}
			enemy.Value.GetComponent<EnemyFormationObeyer>().setFreeFromFormation();
		}
		normalEnemiesInFormation.Clear();
		bossEnemiesInFormation.Clear();
	}

	private void connectFormationMovementComponent()
	{
		if (formationMovement == null)
		{
			formationMovement = gameObject.GetComponent<EnemyFormationMovement>();
			if (formationMovement == null)
			{
				Debug.LogError("Enemy formation manager does not have 'EnemyFormationMovement' -script attached. Attaching it manually.");
				formationMovement = gameObject.AddComponent<EnemyFormationMovement>();
			}
		}
	}


}
