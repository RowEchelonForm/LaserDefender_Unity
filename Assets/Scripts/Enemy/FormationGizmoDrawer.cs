﻿using UnityEngine;
using System.Collections;

public class FormationGizmoDrawer : MonoBehaviour
{
	
	[SerializeField]
	private float pivotRadius = 0.2f;
	
	void OnDrawGizmos()
	{
		float xMinBound = 0;
		float xMaxBound = 0;
		float yMinBound = 0;
		float yMaxBound = 0;
		bool init = true;

		foreach ( Transform child in transform )
		{
			if ( child.tag == "Position" )
			{
				setGizmoBounds(ref init, ref xMinBound, ref xMaxBound, ref yMinBound, ref yMaxBound, child);
			}
		}

		Vector3 mid = new Vector3((xMaxBound+xMinBound)/2, (yMaxBound+yMinBound)/2, 0);
		Gizmos.color = Color.black;
		Gizmos.DrawWireCube( mid, new Vector3(Mathf.Abs(xMaxBound-xMinBound), Mathf.Abs(yMaxBound-yMinBound), 0) );

		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(transform.position, pivotRadius);
	}

	void setGizmoBounds(ref bool init, ref float xMinBound, ref float xMaxBound, ref float yMinBound, ref float yMaxBound, Transform child)
	{
		float padding = child.gameObject.GetComponent<Position>().gizmoRadius;
		if (init)
		{
			xMinBound = child.position.x - padding;
			xMaxBound = child.position.x + padding;
			yMinBound = child.position.y - padding;
			yMaxBound = child.position.y + padding;
			init = false;
			return;	
		}
		if ( child.position.x - padding < xMinBound )	{ xMinBound = child.position.x - padding; }
		if ( child.position.x + padding > xMaxBound )	{ xMaxBound = child.position.x + padding; }
		if ( child.position.y - padding < yMinBound )	{ yMinBound = child.position.y - padding; }
		if ( child.position.y + padding > yMaxBound )	{ yMaxBound = child.position.y + padding; }
	}

}
