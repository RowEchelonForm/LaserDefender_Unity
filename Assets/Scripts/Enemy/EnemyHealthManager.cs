﻿using UnityEngine;
using System.Collections;

public class EnemyHealthManager : MonoBehaviour
{
	public int health { get {return health_;} private set { health_ = value; } }

	[SerializeField]
	private int health_ = 1;

	private bool isDead;

	// Use this for initialization
	void Start()
	{
		healthSafetyCheck();
	}
	
	// Update is called once per frame
	void Update()
	{
		//takeDmgOnKeyPress(); // testing
	}


	public void takeDamage(int dmg)
	{
		health_ -= dmg;
		if ( health_ <= 0 )
		{
			if ( !isDead )
			{
				isDead = true;
				Destroy(gameObject);
			}
		}
	}

	private void healthSafetyCheck()
	{
		if (health_ <= 0)
		{
			isDead = true;
			Debug.Log("No health on spawn, destroying");
			Destroy(gameObject);
		}
		else
		{
			isDead = false;
		}
	}


	void OnTriggerEnter2D(Collider2D coll)
	{
		// Laser hits:
		if (coll.tag == "Laser")
		{
			LaserBeam laser = coll.GetComponent<LaserBeam>();
			if ( laser == null )
			{
				Debug.LogError("An object has a laser tag but doesn't have a 'LaserBeam' component!");
			}
			else
			{
				takeDamage(laser.damage);
				laser.hit();
			}
		}
	}




	//=========================|=========|=========================//
	//=========================| TESTING |=========================//
	//=========================V=========V=========================//
	/*

	private void takeDmgOnKeyPress()
	{
		if ( Input.GetMouseButtonDown(0) )
		{
			takeDamage(1);
		}
	}
	
	*/

}
