﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationFiring : MonoBehaviour
{

	[SerializeField]
	private float firingSpeedUpPercentage = 100f;



	public float getFiringSpeedUp()
	{
		return firingSpeedUpPercentage;
	}
}
