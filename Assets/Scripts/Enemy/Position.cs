﻿using UnityEngine;
using System.Collections;

public class Position : MonoBehaviour
{

	public bool isBossPos { get { return isBossPos_; } private set { isBossPos_ = value; } }
	public GameObject enemyPrefabOverride { get { return enemyPrefabOverride_; } private set { enemyPrefabOverride_ = value; } }
	public float gizmoRadius { get { return gizmoRadius_; } private set { gizmoRadius_ = value; } }

	[SerializeField]
	private GameObject enemyPrefabOverride_;
	[SerializeField]
	private bool isBossPos_ = false;
	[SerializeField]
	private float defaultGizmoRadius = 0.5f;

	private float gizmoRadius_;
	private bool isPosTaken = false;


	public void setPositionAsTaken( bool isTaken )
	{
		isPosTaken = isTaken;
	}

	public bool isPositionTaken()
	{
		return isPosTaken;
	}


	//==============|===============================|==============//
	//==============| GIZMO DRAWING FOR EDITOR VIEW |==============//
	//==============V===============================V==============//

	void OnDrawGizmos()
	{
		calculateGizmoRadius();
		if (isBossPos_)
		{
			Gizmos.color = Color.red;
		}
		else
		{
			Gizmos.color = Color.white;
		}
		Gizmos.DrawWireSphere( gameObject.transform.position, gizmoRadius_ );
	}

	private void calculateGizmoRadius()
	{
		Vector3 spriteHalfSize = new Vector3(defaultGizmoRadius, defaultGizmoRadius, 0);
		EnemyFormationManager formManager = transform.parent.GetComponent<EnemyFormationManager>();
		if ( enemyPrefabOverride_ != null )
		{
			spriteHalfSize = enemyPrefabOverride_.GetComponent<SpriteRenderer>().bounds.extents;
			spriteHalfSize = Vector3.Scale(spriteHalfSize, enemyPrefabOverride_.transform.localScale);
		}
		else if ( formManager != null )
		{
			if (isBossPos_ && formManager.bossEnemyPrefab)
			{
				spriteHalfSize = formManager.bossEnemyPrefab.GetComponent<SpriteRenderer>().bounds.extents;
				spriteHalfSize = Vector3.Scale(spriteHalfSize, formManager.bossEnemyPrefab.transform.localScale);
			}
			else if ( !isBossPos_ && formManager.enemyPrefab )
			{
				spriteHalfSize = formManager.enemyPrefab.GetComponent<SpriteRenderer>().bounds.extents;
				spriteHalfSize = Vector3.Scale(spriteHalfSize, formManager.enemyPrefab.transform.localScale);
			}
		}
		gizmoRadius_ = spriteHalfSize.x;
		if ( gizmoRadius_ < spriteHalfSize.y )
		{
			gizmoRadius_ = spriteHalfSize.y;
		}
	}




}