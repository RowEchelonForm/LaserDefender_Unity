﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The formations in enemyFormations will be deactivated at start. This class will handle their activation.
public class EnemyFormationSpawner : MonoBehaviour
{

	[SerializeField]
	private GameObject[] enemyFormations; // contains the enemy formations in the order they are spawned in
	[SerializeField]
	private const float defaultSpawnDelay = 1f;
	[SerializeField]
	private bool spawnFormationAtStart = true;
	[SerializeField]
	private bool spawnNextFormationAfterEnemiesKilled = true;

	private int spawnedFormationIndex; // index of the formation that was last spawned
	private HashSet<int> currentEnemyIDs; // the enemies from current formation(s) that are alive (instance ids)

	private float spawnTimer = 0f;
	private bool spawnTimerActive = false;


	void Start()
	{
		initFormations();
	}

	void Update()
	{
		/*
		Spawning formations with a delay has to be done like this
		because the animation will glitch for one frame if using a coroutine :/
		*/
		handleSpawnTimer();
	}

	// Spawns the next actual formation after delayToSpawning
	public void spawnNextFormation( float spawnDelay = defaultSpawnDelay )
	{
		
		if (spawnDelay > 0)
		{
			spawnTimer = spawnDelay;
			spawnTimerActive = true;
		}
		else
		{
			spawnNextFormationImmediately();
		}
	}

	// enemyID is instance id of the object
	public void enemyWasSpawned(int enemyID)
	{
		currentEnemyIDs.Add(enemyID);
	}

	// enemyID is instance id of the object
	public void enemyWasDestroyed(int enemyID)
	{
		if ( !currentEnemyIDs.Contains(enemyID) )
		{
			Debug.LogError("Error: EnemyFormationSpawner does not contain an enemy that is being destroyed with the ID: " + enemyID +
						   " but the enemy is still informing this spawner.");
			return;
		}
		currentEnemyIDs.Remove(enemyID);

		if (currentEnemyIDs.Count == 0 && spawnNextFormationAfterEnemiesKilled)
		{
			Debug.Log("All enemies destroyed, spawning next formation.");
			spawnNextFormation();
		}
	}


	private void handleSpawnTimer()
	{
		if (spawnTimerActive)
		{
			spawnTimer -= Time.deltaTime;
			if (spawnTimer <= 0)
			{
				spawnNextFormationImmediately();
				spawnTimerActive = false;
				spawnTimer = 0;
			}
		}
	}

	private void spawnNextFormationImmediately()
	{
		spawnedFormationIndex++;
		if ( spawnedFormationIndex < enemyFormations.Length )
		{
			if ( enemyFormations[spawnedFormationIndex] == null )
			{
				spawnNextFormation(0);
			}
			else
			{
				EnemyFormationManager formationManager = enemyFormations[spawnedFormationIndex].GetComponent<EnemyFormationManager>();
				if ( formationManager.spawnEmpty )
				{
					Debug.LogWarning( "EnemyFormationManager was spawned empty (in EnemyFormationSpawner). " +
									  "If you don't want this, set 'spawnEmpty' property in EnemyFormationManager to 'false'." );
				}
				enemyFormations[spawnedFormationIndex].SetActive(true);
				formationManager.initEnemySpawner(this);
			}
		}
		else
		{
			Debug.Log("All formations in enemySpawner have been spawned and destroyed.");
		}
	}

	private void initFormations()
	{
		spawnedFormationIndex = -1;
		currentEnemyIDs = new HashSet<int>();
		disableAllFormations();
		if (spawnFormationAtStart)
		{
			spawnNextFormation();
		}
	}

	private void disableAllFormations()
	{
		HashSet<int> formationIDs = new HashSet<int>();
		for ( int i = 0; i < enemyFormations.Length; ++i )
		{
			if ( enemyFormations[i] == null )
			{
				Debug.LogWarning("Error in EnemyFormationSpawner: enemyFormations array contians a null gameobject.");
			}
			else if ( enemyFormations[i].tag != "Formation" )
			{
				Debug.LogError( "Error in EnemyFormationSpawner: a formation isn't tagged 'Formation'. " +
								"It will be removed from the 'enemyFormations' array but not destroyed from the game." );
				enemyFormations[i] = null;
			}
			else if ( enemyFormations[i].GetComponent<EnemyFormationManager>() == null )
			{
				Debug.LogError( "Error in EnemyFormationSpawner: a formation doesn't have the 'EnemyFormationManager' component. " +
								"It will be removed from the 'enemyFormations' array but not destroyed from the game." );
				enemyFormations[i] = null;
			}
			else if ( formationIDs.Contains(enemyFormations[i].GetInstanceID()) )
			{
				Debug.LogError( "Error in EnemyFormationSpawner: the same instance of a formation is twice in enemyFormations. " +
								"The second one is discarded. Please create a new instance of the formation (a new gameobject in scene)." );
				enemyFormations[i] = null;
			}
			else
			{
				
				formationIDs.Add( enemyFormations[i].GetInstanceID() );
				enemyFormations[i].SetActive(false);
			}
		}
	}

}
