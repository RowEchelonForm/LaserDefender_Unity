﻿using UnityEngine;
using System.Collections;

public class Shredder : MonoBehaviour
{
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		Destroy(collider.gameObject);
	}




	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube( transform.position, transform.localScale );
	}

}
